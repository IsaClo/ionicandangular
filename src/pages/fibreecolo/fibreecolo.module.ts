import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { FibreecoloPage } from './fibreecolo';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: FibreecoloPage
      }
    ])
  ],
  declarations: [FibreecoloPage]
})
export class FibreecoloPageModule {}
