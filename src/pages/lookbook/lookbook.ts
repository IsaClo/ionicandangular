import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'page-lookbook',
  templateUrl: 'lookbook.html',
  styleUrls: ['lookbook.scss']
})
export class LookbookPage implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/humeurdujour', JSON.stringify(item)]);
  // }
}
