import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'page-humeur',
  templateUrl: 'humeurdujour.html',
  styleUrls: ['humeurdujour.scss']
})
export class HumeurdujourPage implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/humeurdujour', JSON.stringify(item)]);
  // }
}
