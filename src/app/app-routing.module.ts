import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('../pages/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'humeurdujour',
    loadChildren: () => import('../pages/humeurdujour/humeurdujour.module').then(m => m.ListPageModule)
  },
  {
    path: 'lookbook',
    loadChildren: () => import('../pages/lookbook/lookbook.module').then(m => m.LookbookPageModule)
  },
  {
    path: 'fibreecolo',
    loadChildren: () => import('../pages/fibreecolo/fibreecolo.module').then(m => m.FibreecoloPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
